<?php

namespace App\Http\Controllers\Api\Auth;
commit1 commit4

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends ApiController
{
    public function __construct()
    {
        commit2-part1
        commit2-part2
        $this->middleware('commit4');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->response->errorUnauthorized();
                commit2-part3
            }
        } catch (JWTException $e) {
            return $this->response->errorInternal();
                commit2-part4
                commit3
        }

        return $this->response->array(compact('token'));
    }
}

commit5
